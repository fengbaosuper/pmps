package com.jetway.common.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;

import com.jetway.common.dto.BasicDTO;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class Basic implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6950489049199539514L;
	
	public static final int ENABLED_TRUE = 1;
	public static final int ENABLED_FALSE = 0;

	@Id
	@Column(length=32)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderGenerator")
	@SequenceGenerator(name = "orderGenerator", sequenceName = "outpay_order_sequence", allocationSize=1)
	protected Long id;
	
	/**
	 * 1:有效 0:无效
	 */
	@Column
	protected int enabled;
	
	@Column
	protected String remark;
	
	@Column
	protected Long creater;
	
	@Column
	protected Date createDate;
	
	@Column
	protected Long updater;
	
	@Column
	protected Date updateDate;
	
	public abstract BasicDTO to();
}
