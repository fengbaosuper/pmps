package com.jetway.repository.sys;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.jetway.common.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void atest() {
		userRepository.deleteAll();
	}
	
	@Test
	public void btest() {
		List<User> users = userRepository.findAll();
		assertEquals(0, users.size());
	}
	
	@Test
	public void ctest() {
		User user = new User();
		user.setUsername("test1");
		user.setName("陈健");
		user.setEnabled(1);
		userRepository.save(user);
	}
	
	@Test
	public void dtest() {
		List<User> users = userRepository.findAll();
		assertEquals(1, users.size());
	}
	
	@Test
	public void etest() {
		User user = userRepository.getOneByEnabledAndUsername(1, "test1");
		assertNotNull(user);
		
		user = userRepository.getOneByEnabledAndUsername(0, "test1");
		assertNull(user);
	}
	
	@Test
	public void ftest() {
		Pageable pageable = PageRequest.of(0, 10);
		
		Page<User> page = userRepository.findAll(pageable);
		assertEquals(1, page.getTotalElements());
		
		page = userRepository.findAllByEnabled(0, pageable);
		assertEquals(0, page.getTotalElements());
		
		page = userRepository.findAllByEnabled(1, pageable);
		assertEquals(1, page.getTotalElements());
	}
	
}
