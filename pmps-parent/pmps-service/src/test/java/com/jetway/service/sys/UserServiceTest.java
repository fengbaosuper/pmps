package com.jetway.service.sys;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jetway.common.dto.PageDTO;
import com.jetway.common.dto.UserDTO;
import com.jetway.common.model.Basic;
import com.jetway.common.model.User;
import com.jetway.repository.sys.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceTest {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void atest() {
		assertNotNull(userService);
		assertNotNull(userRepository);
		userRepository.deleteAll();
	}
	
	public void btest() {
		PageDTO page = new PageDTO();
		page.setPageNumber(0);
		page.setPageSize(10);
		UserDTO user = new UserDTO();
		user.setEnabled(1);
		page.setUser(user);
		
		page = userService.findPage(page);
		assertNotNull(page);
		assertEquals(0, page.getPageNumber());
		assertEquals(0, page.getTotalPages());
		assertEquals(0, page.getTotalSizes());
	}
	
	public void ctest() {
		User user = new User();
		user.setUsername("test1");
		user.setPwd("test1");
		user.setEnabled(Basic.ENABLED_TRUE);
		Long id = userService.save(user);
		assertNotNull(id);
	}
	
	public void dtest() {
		boolean logined = userService.login("test1", "test1");
		assertTrue(logined);
	}
	
	public void etest() {
		PageDTO page = new PageDTO();
		page.setPageNumber(0);
		page.setPageSize(10);
		UserDTO user = new UserDTO();
		user.setEnabled(1);
		page.setUser(user);
		
		page = userService.findPage(page);
		assertNotNull(page);
		assertEquals(0, page.getPageNumber());
		assertEquals(1, page.getTotalPages());
		assertEquals(1, page.getTotalSizes());
	}
	
}
