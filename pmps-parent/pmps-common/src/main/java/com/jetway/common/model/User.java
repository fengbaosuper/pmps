package com.jetway.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.jetway.common.dto.BasicDTO;
import com.jetway.common.dto.UserDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "sys_user")
public class User extends Basic implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6657856449167780623L;

	@Column
	private String username;
	
	@Column
	private String pwd;
	
	@Column
	private String name;
	
	@Column
	private String mobile;
	
	@Column
	private String mail;
	
	@Column
	private String sn;
	
	@Column
	private Long portrait;
	
	@Column
	private String portraitUrl;

	@Override
	public BasicDTO to() {
		UserDTO user = new UserDTO();
		BeanUtils.copyProperties(this, user);
		return user;
	}
	
}
