package com.jetway.common.dto;

import org.springframework.beans.BeanUtils;

import com.jetway.common.model.User;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BasicDTO<User> {

	private Long id;
	
	private String username;
	
	private String name;
	
	private String mobile;
	
	private String mail;
	
	private String sn;
	
	private Long portrait;
	
	private String portraitUrl;
	
	private int enabled;

	@Override
	public User to() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}
	
}
