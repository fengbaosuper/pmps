package com.jetway.repository.sys;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jetway.common.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	Page<User> findAllByEnabled(int enabled, Pageable pageble);
	
	List<User> findAllByEnabled(int enabled);
	
	User getOneByEnabledAndUsername(int enabled, String username);
	
	boolean existsByUsername(String username);
	
	boolean existsByIdNotAndUsername(Long id, String username);
}
 