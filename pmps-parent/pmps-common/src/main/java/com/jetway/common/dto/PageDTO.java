package com.jetway.common.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import com.jetway.common.model.Basic;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageDTO {

	private int pageNumber;
	
	private int pageSize;
	
	private int totalPages;
	
	private long totalSizes;
	
	/**
	 * 分页查询后，返回的数据
	 */
	private List<BasicDTO> list;
	
	// 用户分页时的查询条件
	private UserDTO user;
	
	public static PageDTO of(Page<? extends Basic> page){
		PageDTO result = new PageDTO();
		
		List<BasicDTO> data = new ArrayList<>();
		List<? extends Basic> list = page.getContent();
		for (Basic basic : list) {
			data.add(basic.to());
		}
		result.setList(data);
		
		result.setPageNumber(page.getPageable().getPageNumber());
		result.setPageSize(page.getPageable().getPageSize());
		result.setTotalPages(page.getTotalPages());
		result.setTotalSizes(page.getTotalElements());
		
		return result;
	}
}
