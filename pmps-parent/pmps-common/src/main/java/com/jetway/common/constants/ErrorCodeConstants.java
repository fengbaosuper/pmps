package com.jetway.common.constants;

public class ErrorCodeConstants {
	
	public static final String ERROR_CODE_SYS_INTERNAL_SERVER = "100001";
	public static final String ERROR_CODE_SYS_NOT_FOUND = "100002";
	
	
	// 用户业务错误
	public static final String ERROR_CODE_USER_USERNAME_EXIST = "101001";		// 用户名已存在
	public static final String ERROR_CODE_USER_LOGIN_FAIL = "101002";			// 有户名或密码错误

}
