package com.jetway.common.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseResult {
	
	// 调用结果：
	// error：失败 
	// success：陈工
	private String result;
	
	// 调用结果为error时，的错误编码
	private String code;

	// 返回的消息
	private String msg;
	
	// 返回的数据，如列表数据，分页数据
	private Object data;
}
