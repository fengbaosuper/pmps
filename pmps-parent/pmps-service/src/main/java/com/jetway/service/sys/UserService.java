package com.jetway.service.sys;

import com.jetway.common.dto.PageDTO;
import com.jetway.common.dto.UserDTO;
import com.jetway.common.model.User;

public interface UserService {

	boolean login(String username, String password);

	Long save(User user);

	boolean update(User user);

	boolean delete(Long id);

	PageDTO findPage(PageDTO page);

}
