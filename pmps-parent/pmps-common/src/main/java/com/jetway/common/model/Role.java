package com.jetway.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jetway.common.dto.BasicDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)

@Entity
@Table(name = "sys_role")
public class Role extends Basic implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4582352543224429736L;

	@Column
	private String name;

	@Override
	public BasicDTO to() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
