package com.jetway.service.sys.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jetway.common.dto.PageDTO;
import com.jetway.common.model.Basic;
import com.jetway.common.model.User;
import com.jetway.repository.sys.UserRepository;
import com.jetway.service.sys.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public boolean login(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Long save(User user) {
		boolean existUser = userRepository.existsByUsername(user.getUsername());
		if (existUser) {
			return null;
		}
		
		user = userRepository.save(user);
		return user.getId();
	}

	@Override
	public boolean update(User user) {
		boolean existUser = userRepository.existsByIdNotAndUsername(user.getId(), user.getUsername());
		if (existUser) {
			return false;
		}
		
		userRepository.save(user);
		return true;
	}

	@Override
	public boolean delete(Long id) {
		User user = userRepository.getOne(id);
		if (user == null) {
			return false;
		}
		
		user.setEnabled(Basic.ENABLED_FALSE);
		userRepository.save(user);
		return true;
	}

	@Override
	public PageDTO findPage(PageDTO page) {
		Example<User> example = null;
		if (page.getUser()!=null) {
			example = Example.of(page.getUser().to());
		}
		
		Pageable pageable = PageRequest.of(page.getPageNumber(), page.getPageSize());
		
		Page<User> result = userRepository.findAll(example, pageable);
		return PageDTO.of(result);
	}
	
	

}
