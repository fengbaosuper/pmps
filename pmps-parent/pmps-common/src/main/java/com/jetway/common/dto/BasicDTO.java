package com.jetway.common.dto;

import java.util.Date;

import com.jetway.common.model.Basic;

import lombok.Data;

@Data
public abstract class BasicDTO<T extends Basic> {
	
	protected int page;
	
	protected int size;
	
	public abstract T to();
	
}